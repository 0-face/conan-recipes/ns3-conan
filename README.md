# ns-3 network simulator - conan package

[Conan.io](https://www.conan.io/) package for [ns-3][ns3_home] libraries.

[ns3_home]: https://www.nsnam.org/

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
[ns3 documentation][ns3_home] to instructions in how to use the ns3 network simulator.

## License

This conan package is distributed under the [unlicense](http://unlicense.org/)
terms (see UNLICENSE.md).

ns3, however, is distributed under the [GNU GPLv2 license](http://www.gnu.org/copyleft/gpl.html).


## Limitations

This package currently does not enable all available options or modules for ns3.
Specially, it does not build yet the python wrappers and optional modules
(that requires external dependencies).

Please, contribute to improve the package.
